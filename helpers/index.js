import { Dimensions, Platform, AsyncStorage, NetInfo } from 'react-native';
import { connect } from 'react-redux';
import DeviceInfo from 'react-native-device-info';
import _ from 'lodash';
import RNFB from 'rn-fetch-blob';

export const serverRoot = 'https://cms.sps-digital.com';
const projectId = 1;
const token = '1234567890';
const deviceId = DeviceInfo.getUniqueID();
const modules = [
    { name: 'projectJson', action: 'getProject' },
    { name: 'contentJson', action: 'getContentByLanguage&languageId=' },
];
global.checkedFiles = { allDownloaded: false, failedDownloads: [] }
let userToken = '';
const dirs = RNFB.fs.dirs;
/**
 * Sets userToken if user is logged in
 */
export const setUserToken = (token) => {
    userToken = token;
}
/**
 * Builds an URL
 * @param {String} action - exp: getProject
 */
export const buildURL = (action) => {
    return serverRoot +
        '/?a=ajax' +
        '&do=' + action +
        '&projectId=' + projectId +
        '&token=' + token +
        '&deviceId=' + deviceId +
        (!!userToken && '&userToken=' + userToken)
}


/**
 * Returns width of the screen
 */
export const width = Dimensions.get('window').width;

/**
 * Returns height of the screen
 */
export const height = Dimensions.get('window').height;

/**
 * Check if Platform is Android
 */
export const isAndroid = () => {
    if (Platform.OS === 'android') {
        return true;
    }
    return false;
}

/**
 * Check if Platform is iOS
 */
export const isIOS = () => {
    if (Platform.OS === 'ios') {
        return true;
    }
    return false;
}

/**
 * Filter files by parametar.
 * @param {Array} files - Array of Files
 * @param {String} filter - Can be enum(['image', 'video', 'document'])
 */

export const filterFiles = (files, filter) => {
    if (files == undefined)
        return [];
    return files.filter(f => f.type === filter);
}

/**
 * Downloads new JSON and saves it, or loads the old one.
 * @param {String} json - Name of JSON to check
 */

export const jsonLogic = async (json, langId = '') => {
    try {
        json = json == 'projectJson' || json == 'contentJson' ? modules.find(m => m.name == json) : global.modules.find(m => m.name == json)
        let newJson = await fetch(buildURL(json.action + langId)).then(res => res.json())
        let oldJson = await AsyncStorage.getItem(json.name)
        global[json.name] = newJson;
        if (!oldJson) {
            console.log('Usao u if')
            await AsyncStorage.setItem(json.name, JSON.stringify(newJson))
            return newJson;
        } else {
            oldJson = JSON.parse(oldJson)
            if (oldJson.lastChanges == global[json.name].lastChanges && json.name == 'projectJson') {
                throw error = 'Project json je isti'
            } else if (oldJson.lastChanges == global[json.name].lastChanges) {
                return newJson;
            } else {
                console.log('Usao u else')
                await AsyncStorage.setItem(json.name, JSON.stringify(newJson));
                return newJson;
            }
        }
    }
    catch (e) {
        console.log('jsonLogic: ' + json.name + ': ' + e)
        if (e == 'Project json je isti') {
            throw error = 'Project json je isti'
        }
    }
}

export const asyncStorageLogic = async (json) => {
    try {
        let asyncJson = await AsyncStorage.getItem(json.name).then(res => JSON.parse(res))
        if (asyncJson)
            global[json.name] = asyncJson;
    } catch (error) {
        console.log('Catch iz asyncStorageLogic: ', error)
    }
}

export const getModules = (json) => {
    global.modules = json.modules.map(m => {
        return { name: m.name + 'Json', action: m.getAction }
    })
}

// export const jsonLogic = (json, langId = '') => {
//     json = modules.find(m => m.name == json);
//     return new Promise((resolve, reject) => {
//         fetch(buildURL(json.action + langId))
//             .then(res => res.json())
//             .then(newJson => {
//                 return new Promise((resolve, reject) => {
//                     AsyncStorage.getItem(json.name)
//                         .then(oldJson => {
//                             global[json.name] = newJson;
//                             if (!oldJson) {
//                                 AsyncStorage.setItem(json.name, JSON.stringify(newJson));
//                                 return Promise.resolve(newJson);
//                             } else {
//                                 oldJson = JSON.parse(oldJson);
//                                 if (oldJson.lastChanges == global[json.name].lastChanges) {
//                                     return Promise.resolve(newJson);
//                                 } else {
//                                     AsyncStorage.setItem(json.name, JSON.stringify(newJson));
//                                     return Promise.resolve(newJson);
//                                 }
//                             }
//                         })
//                         .then(res => resolve(res))
//                 })
//             })
//             .then(res => resolve(res))
//             .catch(err => console.log('jsonLogic(): ' + json.name + ': ' + err))
//     })
// }

export const checkFiles = async () => {
    let niz = global.contentJson.files.map(f => RNFB.fs.exists(dirs.DocumentDir + '/' + f.filename));
    const a = await Promise.all(niz);
    const b = a.map((e, i) => {
        if (!e) {
            return global.contentJson.files[i];
        }
    })
    return b;
}

export const calculateSize = async (filesArr) => {
    let result = 0;
    if (filesArr.length <= 0) {
        return ('Array is empty')
    } else {
        filesArr.forEach(element => {
            if (element) {
                result += Number(element.size);
            }
        });
        result = (result / 1024 / 1024).toFixed(2);
        // this.setState({ visibleDownload: true, total: result });
        let a = await result
        return a;
    }
}

export const processArrayInSequence = (array, fn) => {
    var index = 0;
    return new Promise((resolve, reject) => {
        function next() {
            if (index < array.length) {
                fn(array[index++]).then(next, reject);
            } else {
                resolve();
            }
        }
        next();
    })
}

export const isNetworkConnected = () => {
    if (Platform.OS === 'ios') {
        return new Promise(resolve => {
            const handleFirstConnectivityChangeIOS = isConnected => {
                NetInfo.isConnected.removeEventListener('connectionChange', handleFirstConnectivityChangeIOS);
                resolve(isConnected);
            };
            NetInfo.isConnected.addEventListener('connectionChange', handleFirstConnectivityChangeIOS);
        });
    }
    return NetInfo.isConnected.fetch();
}


export const checkHashFiles = (pocetni) => {
    console.log('usao u hash files()');
    return new Promise((resolve, reject) => {
        let downloadStage = pocetni;
        //console.log('pocetni niz: ' + pocetni);
        // global.checkedFiles.failedDownloads = [];

        AsyncStorage.getItem('supportedLanguages')
            .then(res => JSON.parse(res))
            .then(res => {
                let a = res.currentlySupportedLanguages.map(l => {
                    return new Promise((resolve, reject) => {
                        AsyncStorage.getItem(l.language)
                            .then(res => JSON.parse(res))
                            .then(res => {
                                let b = res.files.map(file => {
                                    return new Promise((resolve, reject) => {
                                        RNFB.fs.exists(dirs.DocumentDir + '/' + file.filename)
                                            .then(res => {
                                                if (!res) { /* && md5(dirs.DocumentDir + '/' + file.fileId + '.' + file.ext)  != file.hash*/
                                                    downloadStage.push(file);
                                                }
                                                return Promise.resolve();
                                            })
                                            .then(() => resolve())
                                    })
                                })
                                Promise.all(b)
                                    .then(() => resolve())
                            })
                    })
                })
                Promise.all(a)
                    .then(() => { downloadStage = _.uniqBy(downloadStage, 'filename'); return Promise.resolve(); })
                    .then(() => resolve(downloadStage))
                    .catch(err => console.log('Greska kod checkHashFiles()' + err))

            })
    })
}

// export const userTokenAndIdLogic = () => {
//     return new Promise((resolve, reject) => {
//         AsyncStorage.getItem('@userToken')
//             .then(res => res ? global.userToken = '&userToken=' + res : null)
//             .then(() => AsyncStorage.getItem('@userId'))
//             .then(res => res ? global.user = res : null)
//             .then(() => {
//                 urls = global.urls = {
//                     projectJson: serverRoot + '/?a=ajax&do=getProject&projectId=' + global.projectId + '&token=1234567890&deviceId=' + deviceId + global.userToken,
//                     usersJson: serverRoot + '/?a=ajax&do=getUsers&projectId=' + global.projectId + '&token=1234567890&deviceId=' + deviceId + global.userToken,
//                     pdfJson: serverRoot + '/?a=ajax&do=getDocuments&projectId=' + global.projectId + '&token=1234567890&deviceId=' + deviceId + global.userToken,
//                     videosJson: serverRoot + '/?a=ajax&do=getVideos&projectId=' + global.projectId + '&token=1234567890&deviceId=' + deviceId + global.userToken,
//                     leafletJson: serverRoot + '/?a=ajax&do=getLeaflets&projectId=' + global.projectId + '&token=1234567890&deviceId=' + deviceId + global.userToken,
//                     notificationsJson: serverRoot + '/?a=ajax&do=getNotifications&projectId=' + global.projectId + '&token=1234567890&deviceId=' + deviceId + global.userToken
//                 };
//             })
//             .then(() => resolve())
//             .catch(e => console.log('Error iz userTokenLogic: ' + e))
//     })
// }
