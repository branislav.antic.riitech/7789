import { MENU1_SELECTED, MENU23_SELECTED, FILTER_PAGES, IS_MENU_OPEN, MENU_SCROLL_1, MENU_SCROLL_2 } from './types';
import { headerChanged } from './HeaderActions';
import { changeSlide } from './BodyActions';

export const changeMenu1 = (menu1Id) => {
    return {
        type: MENU1_SELECTED,
        payload: menu1Id
    }
}

export const changeAndResetMenu1 = (menu1Id) => {
    return (dispatch) => {
        dispatch(changeMenu1(menu1Id))
        dispatch(saveMenuScroll2(0))
    }
}

export const menuClicked = (menuId, index) => {
    return (dispatch) => {
        dispatch(changeSlide(0));
        let thisMenu = global.contentJson.menus[0].menu.find(m => m.menuId == menuId);

        // find menu 1
        let menu1 = menu1Finder(thisMenu);
        let menu1Index = global.contentJson.menuTrees[0].menuTree.findIndex(m => m.menuId == menu1.menuId);
        dispatch(changeMenu1(menu1Index));

        // find menu23
        dispatch(changeMenu23({ menuId, index }));

        // filter pages
        let pages = global.contentJson.pages.filter(p => p.menuId == menuId);
        dispatch(filterPages(pages));

        // close header and menu
        dispatch(headerChanged(''));
        dispatch(isMenuOpen(false));
    }

}

export const filterPages = (pages) => {
    return {
        type: FILTER_PAGES,
        payload: pages
    }
}


export const changeMenu23 = ({ menuId, index }) => {
    return {
        type: MENU23_SELECTED,
        payload: { menuId, index }
    }
}

const menu1Finder = (menu) => {
    if (menu.depth == 1) {
        return menu;
    } else {
        let a = global.contentJson.menus[0].menu.find(m => m.menuId == menu.parentId);
        return menu1Finder(a);
    }
}

export const saveMenuScroll1 = (x) => {
    return {
        type: MENU_SCROLL_1,
        payload: x
    }
}

export const saveMenuScroll2 = (x) => {
    return {
        type: MENU_SCROLL_2,
        payload: x
    }
}

export const isMenuOpen = (bool) => {
    return {
        type: IS_MENU_OPEN,
        payload: bool
    }
}