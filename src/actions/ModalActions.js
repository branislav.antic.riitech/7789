import { SHOW_MODAL, CLOSE_MODAl, WHAT_TO_DOWNLOAD, DOWNLOADING } from './types';
import { AsyncStorage } from 'react-native';
import RNFB from 'rn-fetch-blob';
import DeviceInfo from 'react-native-device-info';
const serverRoot = 'https://cms.sps-digital.com/files/5/';

const deviceId = DeviceInfo.getUniqueID();
const dirs = RNFB.fs.dirs;

// processArrayInSequence = (array, fn) => {
//     var index = 0;
//     return new Promise((resolve, reject) => {
//         function next() {
//             if (index < array.length) {
//                 fn(array[index++]).then(next, reject)
//             } else {
//                 resolve();
//             }
//         }
//         next();
//     })
// }

processArrayInSequence = async (array, fn) => {
    var index = 0;
    next = async () => {
        if (index < array.length) {
            await fn(array[index++])
            next()
        } else {
            return;
        }
    }
    next()
}

downloadFiles = async (filesArr) => {
    console.log('usao u downloadFiles()')
    // return new Promise((resolve, reject) => {
    /*let b = prepareFilesArrayIntoChunks(filesArr, 1);
    let a = b.map(chunk =>
      chunkDownload(chunk)
        .then(() => console.log('zavrsio 5'))
    );*/
    /*let a = filesArr.map(file =>
      downloadOne(file)
    // );*/
    // this.setState({ downloadedL: filesArr.length });
    // recalc = Math.ceil(filesArr.length * recalc_precision);

    await Promise.all(processArrayInSequence(filesArr, downloadOne))
        .then(() => console.log('AAAAA'))

    // .then(() => console.log('ALL downloads finished'))
    // .then(() => console.log('All downloads finished!'))
    // .then(() => filesArr.length >= 511 ? global.checkedFiles.allDownloaded = true : global.checkedFiles.allDownloaded = false)
    // .then(() => AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles)))
    // // .then(() => resolve(true))
    // .catch(err => console.log('Greska kod downloadFIles(): ' + err))


    // })
}

downloadOne = async (file) => {
    let a = RNFB.config({ path: dirs.DocumentDir + '/' + file.filename }).fetch('GET', serverRoot + file.filename)
    .then(r => {
        if (r.info().status == 200) {
            console.log('One file downloaded at ', r.path() + ', with status code: ' + r.info().status)
        } else if (r.info().status == 404) {
            console.log('Fajl ne postoji: ' + file.fileId);
            global.checkedFiles.failedDownloads.push(file);
            AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles));
            RNFB.fs.unlink(dirs.DocumentDir + '/' + file.filename);
            // return resolve();
        } else {
            console.log('Neka druga greska');
            global.checkedFiles.failedDownloads.push(file);
            AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles));
            RNFB.fs.unlink(dirs.DocumentDir + '/' + file.filename);
            // return resolve();
        }
    })
        .catch((err) => {
            console.log('Fajl koruptovan: ' + file.fileId);
            global.checkedFiles.failedDownloads.push(file);
            AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles));
            RNFB.fs.unlink(dirs.DocumentDir + '/' + file.filename);
            // return resolve()
        })

    await a;
    // return dispatch => dispatch({ type: DOWNLOADING })
    // return new Promise((resolve, reject) => {
    //     let t0 = Date.now();
    //     RNFB.config({ path: dirs.DocumentDir + '/' + file.filename }).fetch('GET', serverRoot + file.filename)
    //         .then(r => {
    //             if (r.info().status == 200) {
    //                 console.log('One file downloaded at ', r.path() + ', with status code: ' + r.info().status);
    //                 // store.dispatch({ type: DOWNLOADING })
    //                 let t1 = Date.now();
    //                 this.setState(prevState => ({ downloaded: prevState.downloaded + 1, mbDone: prevState.mbDone + Math.round(Number(file.size) / 1024 / 1024) }));
    //                 let time = t1 - t0;
    //                 let sizeOne = Number(file.size) / 1024.0;
    //                 let dlSpeed = sizeOne / time;
    //                 if (recalc > 0 || global.averageSpeed < 0.66 * dlSpeed || global.averageSpeed * 0.43 > dlSpeed) {
    //                     --recalc;
    //                     if (global.averageSpeed < 0.66 * dlSpeed) {
    //                         console.log('*** HI HIT', global.averageSpeed, dlSpeed);
    //                         global.averageSpeed = 0.04 * dlSpeed + (1 - 0.04) * global.averageSpeed;
    //                         this.setState(() => ({ bonusSec: ((this.state.total - this.state.mbDone) / global.averageSpeed).toFixed(0) }));
    //                     } else if (global.averageSpeed * (0.66 * 0.66) > dlSpeed) {
    //                         console.log('@@@ LOW HIT', global.averageSpeed, dlSpeed);
    //                         if (Math.ceil(this.state.downloadedL * (1 - recalc_precision)) > this.state.downloaded) {
    //                             console.log('NOT LAST FILES', Math.ceil(this.state.downloadedL * (1 - recalc_precision), Math.ceil(this.state.downloadedL * (1 - recalc_precision) < this.state.downloaded)))
    //                             global.averageSpeed = 0.04 * dlSpeed + (1 - 0.04) * global.averageSpeed;
    //                             this.setState(() => ({ bonusSec: ((this.state.total - this.state.mbDone) / global.averageSpeed).toFixed(0) }));
    //                         }

    //                     }
    //                 }
    //                 return resolve();
    //             } else if (r.info().status == 404) {
    //                 console.log('Fajl ne postoji: ' + file.fileId);
    //                 global.checkedFiles.failedDownloads.push(file);
    //                 AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles));
    //                 RNFB.fs.unlink(dirs.DocumentDir + '/' + file.filename);
    //                 return resolve();
    //             } else {
    //                 console.log('Neka druga greska');
    //                 global.checkedFiles.failedDownloads.push(file);
    //                 AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles));
    //                 RNFB.fs.unlink(dirs.DocumentDir + '/' + file.filename);
    //                 return resolve();
    //             }

    //         })
    //         .catch((err) => {
    //             console.log('Fajl koruptovan: ' + file.fileId);
    //             global.checkedFiles.failedDownloads.push(file);
    //             AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles));
    //             RNFB.fs.unlink(dirs.DocumentDir + '/' + file.filename);
    //             return resolve()
    //         })
    // })
}

// downloadFiles = (filesArr) => {
//     console.log('usao u downloadFiles()')
//     return new Promise((resolve, reject) => {
//         /*let b = prepareFilesArrayIntoChunks(filesArr, 1);
//         let a = b.map(chunk =>
//           chunkDownload(chunk)
//             .then(() => console.log('zavrsio 5'))
//         );*/
//         /*let a = filesArr.map(file =>
//           downloadOne(file)
//         // );*/
//         // this.setState({ downloadedL: filesArr.length });
//         // recalc = Math.ceil(filesArr.length * recalc_precision);

//         processArrayInSequence(filesArr, downloadOne)
//             .then(() => console.log('All downloads finished!'))
//             .then(() => filesArr.length >= 511 ? global.checkedFiles.allDownloaded = true : global.checkedFiles.allDownloaded = false)
//             .then(() => AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles)))
//             // .then(() => resolve(true))
//             .catch(err => console.log('Greska kod downloadFIles(): ' + err))


//     })
// }

export const showModal = (mb, niz) => {
    return {
        type: SHOW_MODAL,
        payload: ({ size: mb, niz })
    }
}

export const closeModal = () => {
    return {
        type: CLOSE_MODAl
    }
}

export const downloadInfo = ({ array }) => {
    return dispatch => {
        this.downloadFiles(array)
    }
}


