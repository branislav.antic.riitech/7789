import { CHANGE_SLIDE } from './types';


export const changeSlide = (index) => {
    return {
        type: CHANGE_SLIDE,
        payload: index
    }
}