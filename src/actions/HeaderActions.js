import { HEADER_SELECTED, HEADER_OPEN } from './types';

export const headerChanged = (whatIsOpen) => {
    return {
        type: HEADER_SELECTED,
        payload: whatIsOpen
    }
}