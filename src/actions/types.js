export const HEADER_SELECTED = 'header_selected';
export const HEADER_OPEN = 'header_open';
export const MENU1_SELECTED = 'menu1_selected';
export const MENU23_SELECTED = 'menu23_selected';
export const FILTER_PAGES = 'filter_pages';
export const IS_MENU_OPEN = 'is_menu_open';
export const MENU_SCROLL_1 = 'menu_scroll_1';
export const MENU_SCROLL_2 = 'menu_scroll_2';
export const CHANGE_SLIDE = 'change_slide';
export const SHOW_MODAL = 'show_modal';
export const CLOSE_MODAl = 'close_modal';
export const WHAT_TO_DOWNLOAD = 'what_to_download';
export const DOWNLOADING = 'downloading';