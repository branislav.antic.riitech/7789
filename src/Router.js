import React from 'react';
import { StatusBar } from 'react-native';
import { Scene, Router, Actions, Stack } from 'react-native-router-flux';
import Test from './components/Test';
import Header from './components/Header'
import Loading from './components/Loading';

const RouterComponent = () => {
    return (
        <Router>
            <Stack key='root'>
                <Stack hideNavBar key='loading'>
                    <Scene key='loading' component={Loading} title='Loading' />
                </Stack>

                <Stack navBar={Header} key='main' >
                    <Scene hideNavBar key='test' component={Test} title='Test' initial gesturesEnabled={false}/>
                </Stack>

            </Stack>
        </Router>
    );
}

export default RouterComponent;