import { combineReducers } from 'redux';
import HeaderReducer from './HeaderReducer';
import MenuReducer from './MenuReducer';
import PagesReducer from './PagesReducer';
import BodyReducer from './BodyReducer';

export default combineReducers({
    header: HeaderReducer,
    menus: MenuReducer,
    pages: PagesReducer,
    body: BodyReducer
})