import { CHANGE_SLIDE } from '../actions/types';

const INITIAL_STATE = {
    activeSlide: 0
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CHANGE_SLIDE:
            return { ...state, activeSlide: action.payload };
        default: 
            return state;
    }
}