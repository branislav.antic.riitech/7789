import { FILTER_PAGES } from '../actions/types';

const INITIAL_STATE = {
    pages: []
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case FILTER_PAGES:
            return { ...state, pages: action.payload }
        default:
            return state;
    }
}