import { MENU1_SELECTED, MENU23_SELECTED, IS_MENU_OPEN, MENU_SCROLL_1, MENU_SCROLL_2 } from '../actions/types';

const INITIAL_STATE = {
    menu1Selected: 0,
    menu23Selected: '',
    menu2Index: 0,
    isMenuOpen: false,
    menuScroll1: 0,
    menuScroll2: 0
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case MENU1_SELECTED:
            return { ...state, menu1Selected: action.payload };
        case MENU23_SELECTED:
            let { menuId, index } = action.payload;
            return { ...state, menu23Selected: menuId, menu2Index: index };
        case IS_MENU_OPEN:
            return { ...state, isMenuOpen: action.payload }
        case MENU_SCROLL_1:
            return { ...state, menuScroll1: action.payload }
        case MENU_SCROLL_2:
            return { ...state, menuScroll2: action.payload }
        default:
            return state;
    }
}