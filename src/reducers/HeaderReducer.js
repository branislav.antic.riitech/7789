import { HEADER_SELECTED, HEADER_OPEN } from '../actions/types';

const INITIAL_STATE = {
    whatIsOpen: '',
    title: ''
}

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case HEADER_SELECTED:
            // check if it's already clicked
            let whatIsOpen = state.whatIsOpen == action.payload ? '' : action.payload;
            return { ...state, whatIsOpen };
        default: 
            return state;
    }
}