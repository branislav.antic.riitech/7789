import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
//import Carousel, { Pagination } from 'react-native-snap-carousel';
import Swiper from 'react-native-swiper';
import { Template1, Template2, Template3, Template4, Template5 } from './Templates';
import { changeSlide } from '../actions';
import { width, filterFiles } from '../../helpers';
import { IndicatorViewPager, PagerDotIndicator } from 'rn-viewpager';
import Language from '../components/Languages';


class Body extends Component {


    constructor() {
        super();

    }

    filterBody = () => {
        return this.props.pages.map((item, i) => {
            switch (item.templateId) {
                case '1':
                    return <View key={i} style={{ flex: 1 }}><Template1 data={item} /></View>
                case '2':
                    return <View key={i} style={{ flex: 1 }}><Template2 data={item} /></View>
                case '3':
                    return <View key={i} style={{ flex: 1 }}><Template3 data={item} /></View>
                case '4':
                    return <View key={i} style={{ flex: 1 }}><Template4 data={item} /></View>
                case '5':
                    return <View key={i} style={{ flex: 1 }}><Template5 data={item} /></View>
                default:
                    console.log('There is no template with that number')
            }
        })
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.pages[0] != this.props.pages[0])
            if (this.props.pages[0].menuId !== prevProps.pages[0].menuId && !this.props.isPresentation) {
                this.scrollView.setPageWithoutAnimation(0)
            }
    }

    filterHeader = () => {
        switch (this.props.header.whatIsOpen) {
            case 'language':
                return <Language />
            default:
                console.log('Otvoren je home')
        }
    }

    render() {
        // console.log(this.props.header.whatIsOpen)
        return (
            <View style={styles.mainContainer}>
                {/* {[this.props.header.whatIsOpen] && <this.props.header.whatIsOpen />} */}
                {/* {this.props.header.whatIsOpen && this.filterHeader()} */}
                <IndicatorViewPager
                    ref={ref => this.scrollView = ref}  
                    initialPage={this.props.activeSlide}
                    onPageSelected={({ position }) => this.props.changeSlide(position)}
                    style={{ flex: 1 }}
                    indicator={this.pagerDotIndicator()}
                >
                    {this.filterBody()}
                </IndicatorViewPager>

            </View>
        );
    }

    pagerDotIndicator = () => {
        return <PagerDotIndicator
            pageCount={this.props.pages.length}
            dotStyle={{
                width: 18,
                height: 18,
                borderRadius: 9,
                margin: 3,
                backgroundColor: 'blue'
            }}
            selectedDotStyle={{
                width: 18,
                height: 18,
                borderRadius: 9,
                margin: 3,
                backgroundColor: 'white'
            }}
            hideSingle
        />
    }

}

const styles = {
    mainContainer: {
        flex: 1,
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        pages: state.pages.pages,
        activeSlide: state.body.activeSlide,
        header: state.header
    }
}

export default connect(mapStateToProps, { changeSlide })(Body);


/*
FOR CAROUSEL

    renderItem = (item, index) => {
        switch (item.templateId) {
            case '1':
                return <Template1 data={item} />
            case '2':
                return <Template2 data={item} />
            case '3':
                return <Template3 data={item} />
            case '4':
                return <Template4 data={item} />
            case '5':
                return <Template5 data={item} />
            default:
                console.log('There is no template with that number')
        }
    }



    render() {
        return (
            <View style={styles.mainContainer}>
                <Carousel
                    ref={(c) => { this.carousel = c; }}
                    data={this.props.pages}
                    renderItem={({ item, index }) => this.renderItem(item, index)}
                    itemWidth={width}
                    sliderWidth={width}
                    onSnapToItem={index => this.props.changeSlide(index)}
                />
                <Pagination
                    dotsLength={this.props.pages.length}
                    activeDotIndex={this.props.activeSlide}
                />
            </View>
        )
    }

                <Swiper
                    loop={false}
                    //onIndexChanged={(i) => this.props.changeSlide(i)}
                    //index={this.props.activeSlide}
                >
                    {this.filterBody()}
                </Swiper>

*/