import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import { isMenuOpen } from '../actions';
import { height } from '../../helpers';

class Footer extends Component {

    constructor() {
        super();
        this.iconSize = height * 0.035;
    }

    render() {
        return (
            <View style={[styles.mainContainer, this.props.style]}>
                <TouchableOpacity onPress={() => { this.props.isMenuOpen(!this.props.menu) }} style={{ width: 30 }}>
                    <Icon name='bars' size={this.iconSize} color='black' />
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = {
    mainContainer: {
        justifyContent: 'center',
        paddingLeft: 15,
        height: 50,

    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        menu: state.menus.isMenuOpen
    }
}

export default connect(mapStateToProps, { isMenuOpen })(Footer);