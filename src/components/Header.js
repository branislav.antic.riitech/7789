import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, TouchableWithoutFeedback, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import { headerChanged, isMenuOpen } from '../actions';
import { height } from '../../helpers';

class Header extends Component {

    constructor() {
        super();
        this.iconSize = height * 0.035;
    }

    whatIsActive = (key) => {
        if (key === this.props.whatIsOpen) {
            return 'red';
        }
        return 'black';
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <StatusBar hidden />
                <View style={styles.innerContainer}>

                    <View style={styles.headerTitle}>
                        <Text style={styles.headerText}>HEADER</Text>
                    </View>


                    <View style={styles.iconsContainer}>

                        {/* LANGUAGE */}
                        <TouchableWithoutFeedback onPress={() => { this.props.headerChanged('language'); console.log(this.props.activeSlide) }}>
                            <View style={styles.touchable}>
                                <Icon style={styles.ico} name='globe' size={this.iconSize} color={this.whatIsActive('language')} />
                            </View>
                        </TouchableWithoutFeedback>
                        
                        {/* BREADCRUMB */}
                        <TouchableWithoutFeedback onPress={() => { this.props.headerChanged('breadcrumb') }}>
                            <View style={styles.touchable}>
                                <Icon style={styles.ico} name='info' size={this.iconSize} color={this.whatIsActive('breadcrumb')} />
                            </View>
                        </TouchableWithoutFeedback>

                        {/* HOME */}
                        <TouchableWithoutFeedback onPress={() => { this.props.headerChanged('home') }}>
                            <View style={styles.touchable}>
                                <Icon style={styles.ico} name='home' size={this.iconSize} color={this.whatIsActive('home')} />
                            </View>
                        </TouchableWithoutFeedback>

                        {/* PRESENTATION */}
                        <TouchableWithoutFeedback onPress={() => { this.props.headerChanged('presentation') }}>
                            <View style={styles.touchable}>
                                <Icon style={styles.ico} name='th' size={this.iconSize} color={this.whatIsActive('presentation')} />
                            </View>
                        </TouchableWithoutFeedback>

                        {/* LEAFLETS */}
                        <TouchableWithoutFeedback onPress={() => { this.props.headerChanged('leaflets') }}>
                            <View style={styles.touchable}>
                                <Icon style={styles.ico} name='file-o' size={this.iconSize} color={this.whatIsActive('leaflets')} />
                            </View>
                        </TouchableWithoutFeedback>

                        {/* SEARCH */}
                        <TouchableWithoutFeedback onPress={() => { this.props.headerChanged('search') }}>
                            <View style={styles.touchable}>
                                <Icon style={styles.ico} name='search' size={this.iconSize} color={this.whatIsActive('search')} />
                            </View>
                        </TouchableWithoutFeedback>

                        {/* VIDEO */}
                        <TouchableWithoutFeedback onPress={() => { this.props.headerChanged('videoList') }}>
                            <View style={styles.touchable}>
                                <Icon style={styles.ico} name='play-circle-o' size={this.iconSize} color={this.whatIsActive('videoList')} />
                            </View>
                        </TouchableWithoutFeedback>

                        {/* PDF */}
                        <TouchableWithoutFeedback onPress={() => { this.props.headerChanged('pdfList') }}>
                            <View style={styles.touchable}>
                                <Icon style={styles.ico} name='folder-open' size={this.iconSize} color={this.whatIsActive('pdfList')} />
                            </View>
                        </TouchableWithoutFeedback>

                        {/* NOTIFICATION */}
                        <TouchableWithoutFeedback onPress={() => { this.props.headerChanged('notification') }}>
                            <View style={styles.touchable}>
                                <Icon style={styles.ico} name='bell-o' size={this.iconSize} color={this.whatIsActive('notification')} />
                            </View>
                        </TouchableWithoutFeedback>

                        {/* SETTINGS */}
                        <TouchableWithoutFeedback onPress={() => { this.props.headerChanged('settings') }}>
                            <View style={styles.touchable}>
                                <Icon style={styles.ico} name='cog' size={this.iconSize} color={this.whatIsActive('settings')} />
                            </View>
                        </TouchableWithoutFeedback>

                    </View>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: { flex: 0.075, backgroundColor: 'white' },
    innerContainer: { flex: 1, flexDirection: 'row', alignItems: 'center' },
    headerTitle: { flex: 1.5, alignItems: 'center' },
    headerText: { fontSize: 18 },
    iconsContainer: { flex: 1, flexDirection: 'row', },
    touchable: { flex: 1, alignItems: 'center' },
    ico: {}
})

const mapStateToProps = (state, ownProps) => {
    return {
        whatIsOpen: state.header.whatIsOpen,
        activeSlide: state.body.activeSlide
    }
}

export default connect(mapStateToProps, { headerChanged })(Header);