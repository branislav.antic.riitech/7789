import React, { Component } from 'react';
import { View, Text, Animated, NetInfo } from 'react-native';
import { connect } from 'react-redux'
import MenuList from './Menu/MenuList';
import Footer from './Footer';
import Body from './Body';
import Header from './Header';


class Test extends Component {

    render() {
        
        return (
            <View style={styles.mainContainer}>
                <View>
                    <Header />
                </View>
                <View style={{ flex: 1, backgroundColor: 'white' }}>
                    <Body />
                </View>


                {!this.props.isMenuOpen && 
                    <View style={styles.footerContainer}>
                        <Footer />
                    </View>
                }
                {this.props.isMenuOpen &&
                    <View style={styles.footerContainerOpen}>
                        <Footer style={{ height: '12%' }} />
                        <MenuList />
                    </View>
                }
            </View>
        )
    }
}

const styles = {
    mainContainer: {
        flex: 1,
        position: 'relative',
        width: '100%',
        height: '100%',
        paddingBottom: 50
    },
    footerContainer: {
        position: 'absolute',
        bottom: 0,
    },
    footerContainerOpen: {
        height: '60%', 
        backgroundColor: 'white',
        position: 'absolute',
        bottom: 0,
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        isMenuOpen: state.menus.isMenuOpen
    }
}

export default connect(mapStateToProps)(Test);