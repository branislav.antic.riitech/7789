import React, { Component } from 'react';
import { View, Text, StyleSheet, AsyncStorage, NetInfo } from 'react-native';
import { TextLoader } from 'react-native-indicator';
import { Actions } from 'react-native-router-flux';
import { jsonLogic, checkFiles, calculateSize, isNetworkConnected, asyncStorageLogic, getModules, serverRoot, processArrayInSequence } from '../../helpers';
import { showModal, downloading1 } from '../actions';
import { connect } from 'react-redux';
import * as Progress from 'react-native-progress';
import base64 from 'base64-js';
import DownloadModal from './DownloadModal';
import RNFB from 'rn-fetch-blob';

const dirs = RNFB.fs.dirs;
let recalc = 1;
const recalc_precision = 0.125;
let averageSpeed;

class Loading extends Component {

    state = {
        isVisible: false,
        downloadModalMessage: '',
        downloadModalMessage2: '',
        downloadModalMessage3: '',
        buttons: [],
        indeterminate: true,
        visibleDownload: false,
        downloadedL: 0,
        downloaded: 0,
        visibleDownloadError: false,
        mbDone: 0,
        total: 0,
        bonusSec: 0
    }

    calcProgress() {
        if (this.state.downloaded == 1) {
            this.state.indeterminate = false;
        }
        if (this.state.downloaded > 0) {
            return this.state.downloaded / this.state.downloadedL;
        }
    }

    setTimerClick = (secondsLeft) => {
        this.setState(() => ({ bonusSec: secondsLeft }));
        let interval = setInterval(() => {
            if (this.state.isLoading == 0) {
                clearInterval(interval);
            }
            this.setState((oldState) => ({ bonusSec: oldState.bonusSec - 1 }));
        }, 1000);
    }

    downloadFiles = (filesArr) => {
        console.log('usao u downloadFiles()')
        return new Promise((resolve, reject) => {
            this.setState({ downloadedL: filesArr.length });
            recalc = Math.ceil(filesArr.length * recalc_precision);

            processArrayInSequence(filesArr, this.downloadOne)
                .then(() => console.log('All downloads finished!'))
                .then(() => filesArr.length = this.length ? global.checkedFiles.allDownloaded = true : global.checkedFiles.allDownloaded = false)
                .then(() => AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles)))
                .then(() => resolve(true))
                .then(() => Actions.main())
                .catch(err => console.log('Greska kod downloadFIles(): ' + err))


        })
    }


    alertForDownload = async (mb, niz) => {
        if (mb == 0) {
            throw error = 'Niz je prazan'
        }
        let connectionInfo = await isNetworkConnected();
        if (connectionInfo) {
            const speedBenchmarkFile = serverRoot + '/' + global.projectJson.project.speedBenchmarkFile;
            const pathToSpeedBenchmarkFile = dirs.DocumentDir + '/benchmark666.jpg';
            const timeBeforeDownload = Date.now();
            const benchmarkFile = await RNFB.config({ path: pathToSpeedBenchmarkFile }).fetch('GET', speedBenchmarkFile)
            const timeAfterDownload = Date.now();
            let benchmarkTime = timeAfterDownload - timeBeforeDownload;
            const data = await RNFB.fs.readFile(benchmarkFile.path(), 'base64')
            const Bytes = base64.byteLength(data);
            const Bits = Bytes * 8;
            benchmarkTime = benchmarkTime > 500 ? benchmarkTime : (benchmarkTime * 80);
            const KBitsPerSecond = Bits / benchmarkTime;
            const MBitsPerSecond = KBitsPerSecond / 1024;
            let downloadSpeed = MBitsPerSecond;
            averageSpeed = downloadSpeed;
            // let est = downloadSpeed != 0 ? (mb / downloadSpeed / 60).toFixed(0) + ' minutes ' + ((mb / downloadSpeed).toFixed(0) % 60) + ' seconds' : 'inf.';
            await RNFB.fs.unlink(pathToSpeedBenchmarkFile)
            if (niz) {
                global.dlTypes = {
                    image: { files: niz.filter(f => f ? f.type == 'image' : null), checked: true },
                    video: { files: niz.filter(f => f ? f.type == 'video' : null), checked: true },
                    document: { files: niz.filter(f => f ? f.type == 'document' : null), checked: true },
                    canva: { files: niz.filter(f => f ? f.type == 'webpage' : null), checked: true }
                }
                return new Promise((resolve, reject) => {
                    this.setState({
                        downloadModalMessage: 'Select what you want to download: ',
                        downloadModalMessage2: global.dlTypes,
                        downloadModalMessage3: downloadSpeed,
                        isVisible: true,
                        buttons: [{
                            naziv: 'Download',
                            akcija: (result, array, failedArray) => { this.setTimerClick((result / downloadSpeed).toFixed(0)); this.setState({ isVisible: false, visibleDownload: true, total: result }); global.checkedFiles.failedDownloads = failedArray; resolve(array) }
                        },
                        {
                            naziv: 'Skip',
                            akcija: () => {
                                global.checkedFiles.allDownloaded = false;
                                global.checkedFiles.failedDownloads = niz;
                                AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles))
                                this.setState({ isVisible: false })
                                reject('Pritisnut reject')
                            }
                        }]
                    })
                })

            }

        } else {
            throw new Error('NEMA NETA')
        }
    }

    downloadOne = async (file) => {
        t0 = Date.now()
        try {
            let a = await RNFB.config({ path: dirs.DocumentDir + '/' + file.filename }).fetch('GET', serverRoot + '/' + global.projectJson.project.contentDir + file.filename)
            if (a.info().status == 200) {
                console.log('One file downloaded at ', a.path() + ', with status code: ' + a.info().status)
                let t1 = Date.now();
                this.setState(prevState => ({ downloaded: prevState.downloaded + 1, mbDone: prevState.mbDone + Math.round(Number(file.size) / 1024 / 1024) }));
                let time = t1 - t0;
                let sizeOne = Number(file.size) / 1024.0;
                let dlSpeed = sizeOne / time;

                if (recalc > 0 || averageSpeed < 0.66 * dlSpeed || averageSpeed * 0.43 > dlSpeed) {
                    --recalc;
                    if (averageSpeed < 0.66 * dlSpeed) {
                        console.log('*** HI HIT', averageSpeed, dlSpeed);
                        averageSpeed = 0.03 * dlSpeed + (1 - 0.04) * averageSpeed;
                        this.setState(() => ({ bonusSec: ((this.state.total - this.state.mbDone) / averageSpeed).toFixed(0) }));
                    } else if (averageSpeed * (0.66 * 0.66) > dlSpeed) {
                        console.log('@@@ LOW HIT', averageSpeed, dlSpeed);
                        if (Math.ceil(this.state.downloadedL * (1 - recalc_precision)) > this.state.downloaded) {
                            console.log('NOT LAST FILES', Math.ceil(this.state.downloadedL * (1 - recalc_precision), Math.ceil(this.state.downloadedL * (1 - recalc_precision) < this.state.downloaded)))
                            averageSpeed = 0.04 * dlSpeed + (1 - 0.04) * averageSpeed;
                            this.setState(() => ({ bonusSec: ((this.state.total - this.state.mbDone) / averageSpeed).toFixed(0) }));
                        }

                    }
                }
            } else if (a.info().status == 404) {
                console.log('Fajl ne postoji: ' + file.fileId);
                global.checkedFiles.failedDownloads.push(file);
                AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles));
                RNFB.fs.unlink(dirs.DocumentDir + '/' + file.filename);
                // return resolve();
            } else {
                console.log('Neka druga greska');
                global.checkedFiles.failedDownloads.push(file);
                AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles));
                RNFB.fs.unlink(dirs.DocumentDir + '/' + file.filename);
                // return resolve();
            }
        } catch (err) {
            // console.log(err)
            console.log('Fajl koruptovan: ' + file.fileId);
            this.setState({ visibleDownloadError: true })
            global.checkedFiles.failedDownloads.push(file);
            AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles));
            RNFB.fs.unlink(dirs.DocumentDir + '/' + file.filename);
            // return resolve()
        }

        // .then(r => {
        //     if (r.info().status == 200) {
        //         console.log('One file downloaded at ', r.path() + ', with status code: ' + r.info().status)
        //         let t1 = Date.now();
        //         this.setState(prevState => ({ downloaded: prevState.downloaded + 1, mbDone: prevState.mbDone + Math.round(Number(file.size) / 1024 / 1024) }));
        //         let time = t1 - t0;
        //         let sizeOne = Number(file.size) / 1024.0;
        //         let dlSpeed = sizeOne / time;

        //         if (recalc > 0 || averageSpeed < 0.66 * dlSpeed) {
        //             --recalc;
        //             averageSpeed = 0.04 * dlSpeed + (1 - 0.04) * averageSpeed;
        //             this.setState(() => ({ bonusSec: ((this.state.total - this.state.mbDone) / averageSpeed).toFixed(0) }));
        //         }

        //     } else if (r.info().status == 404) {
        //         console.log('Fajl ne postoji: ' + file.fileId);
        //         global.checkedFiles.failedDownloads.push(file);
        //         AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles));
        //         RNFB.fs.unlink(dirs.DocumentDir + '/' + file.filename);
        //         // return resolve();
        //     } else {
        //         console.log('Neka druga greska');
        //         global.checkedFiles.failedDownloads.push(file);
        //         AsyncStorage.setItem('checkedFiles', JSON.stringify(global.checkedFiles));
        //         RNFB.fs.unlink(dirs.DocumentDir + '/' + file.filename);
        //         // return resolve();
        //     }
        // }) 
    }

    // shouldIgoToCatch = () => {
    //     return new Promise((resolve, reject) => {
    //       AsyncStorage.getItem('checkedFiles')
    //         .then(res => JSON.parse(res))
    //         .then(res => {
    //           if (global.isProjectJsonSame && res.allDownloaded) {
    //             AsyncStorage.getItem(global.defaultLanguageObject.language)
    //               .then(res => global.globalJson = JSON.parse(res))
    //               .then(() => AsyncStorage.getItem('usersJson'))
    //               .then(res => global.usersJson = JSON.parse(res))
    //               .then(() => AsyncStorage.getItem('pdfJson'))
    //               .then(res => { global.pdfJson = JSON.parse(res); return Promise.resolve() })
    //               .then(() => AsyncStorage.getItem('videosJson'))
    //               .then(res => { global.videosJson = JSON.parse(res); return Promise.resolve(); })
    //               //.then(() => AsyncStorage.getItem('notificationsJson'))
    //               //.then(res => { global.notificationsJson = JSON.parse(res); return Promise.resolve(); })
    //               .then(() => reject('reject iz shouldIgoToCatch'))
    //               .catch((err) => { console.log('shouldIgoToCatch: ' + err); return resolve('shouldIgoToCatch') })
    //           } else {
    //             return resolve('shouldIgoToCatch: projectJson is NEW!');
    //           }
    //         })
    //     })
    //   }


    akoImaNeta = async () => {
        try {
            const projectJson = await jsonLogic('projectJson')
            const contentJson = await jsonLogic('contentJson', projectJson.project.defaultLanguageId)
            await getModules(projectJson)
            const allModules = global.modules.map(m => {
                return jsonLogic(m.name)
            })
            await Promise.all(allModules)
            const niz = await checkFiles(contentJson)
            const mb = await calculateSize(niz)
            const arrayToDl = await this.alertForDownload(mb, niz)
            this.downloadFiles(arrayToDl)
        } catch (err) {
            console.log('===', err)
            if (err === 'Pritisnut reject') {
                console.log('Catch od glavnog bloka akoImaNeta(): ' + err)
                Actions.main()
            } else if (err == 'Project json je isti') {
                try {
                    await getModules(global.projectJson)
                    let allGlobals = global.modules.map(modul => {
                        return asyncStorageLogic(modul);
                    })
                    await Promise.all(allGlobals)
                    const niz = await checkFiles(global.contentJson)
                    const mb = await calculateSize(niz)
                    const arrayToDl = await this.alertForDownload(mb, niz)
                    this.downloadFiles(arrayToDl)
                } catch (error) {
                    console.log('Catch od project json je isti: ' + error)
                    if (error == 'Niz je prazan') {
                        Actions.main()
                    }
                }
            }
        }
        // jsonLogic('projectJson')
        //     .then(res => jsonLogic('contentJson', res.project.defaultLanguageId))
        //     .then(res => checkFiles(res))
        //     .then(niz => calculateSize(niz)
        //         .then(size => this.alertForDownload(size, niz))
        //         .then(arrayToDl => this.downloadFiles(arrayToDl))
        //     )
        //     .catch((err) => {
        //         console.log(err)
        //         if (err === 'Pritisnut reject' || err == 'Niz je prazan') {
        //             console.log('Catch od glavnog bloka akoImaNeta(): ' + err)
        //             Actions.main()
        //         }
        //     })

    }

    akoNemaNeta = async () => {
        try {
            await this.zaNemaNetaDefault()
            console.log('Nema neta defult prosao')
            console.log(global.defaultLanguageObject)
            Actions.main()
        } catch (error) {
            console.log('Catch od akoNeamNeta(): ', error)
        }
    }

    startLogic = async () => {
        const a = await isNetworkConnected()
        if (a) {
            this.akoImaNeta()
        } else {
            this.akoNemaNeta()
        }

    }

    zaNemaNetaDefault = async () => {
        let projectJson = await AsyncStorage.getItem('projectJson');
        if (!projectJson) {
            throw error = 'Ne postoji projectJson u lokalu';
        } else {
            projectJson = await JSON.parse(projectJson)
            global.projectJson = projectJson;
            defaultLanguageId = Number(global.projectJson.project.defaultLanguageId);
            !global.languageId ? global.languageId = defaultLanguageId : false;
            global.defaultLanguageObject = global.projectJson.languages.find(l => l.languageId == defaultLanguageId);
            console.log('Nema neta defult prosao kroz funkciju')
            return;
        }
        // return new Promise((resolve, reject) => {
        //     AsyncStorage.getItem('projectJson')
        //         .then(res => res != null ? res : Promise.reject('Ne postoji project JSON u lokalu'))
        //         .then(res => JSON.parse(res))
        //         .then(res => {
        //             global.projectJson = res;
        //             defaultLanguageId = Number(global.projectJson.project.defaultLanguageId);
        //             !global.languageId ? global.languageId = defaultLanguageId : false;
        //             global.defaultLanguageObject = global.projectJson.languages.find(l => l.languageId == defaultLanguageId);
        //             return Promise.resolve();
        //         })
        //         .then(() => resolve())
        //         .catch(err => reject(err));
        // })
    }


    componentDidMount() {
        this.startLogic()
    }

    render() {
        // console.log(this.props.totalSize)
        return (
            <View style={styles.mainContainer}>

                <View style={styles.textHolderStyle}>
                    {!this.state.visibleDownload && <TextLoader text={'Loading, please wait'} textStyle={styles.loadTextF} />}
                </View>
                {this.state.visibleDownload && <View style={styles.downloadInfo}>
                    {this.state.visibleDownload && <Text style={styles.loadTextF}>Downloaded {this.state.downloaded} of {this.state.downloadedL} files.</Text>}
                    {this.state.visibleDownload && <Text style={styles.loadTextF}>Downloaded {this.state.mbDone} MB of {this.state.total} MB.</Text>}
                    {this.state.visibleDownload &&
                        <Text style={styles.loadTextF}
                        >
                            Remaining time: {averageSpeed &&
                                (this.state.bonusSec >= 60 ?
                                    (Math.round(this.state.bonusSec / 60)).toFixed(0) + ' min' :
                                    (this.state.bonusSec <= 0 ? 'almost done...' : (this.state.bonusSec + ' seconds'))
                                )}
                        </Text>}
                    {this.state.visibleDownloadError && <Text style={[styles.loadTextF, { fontSize: 14 }]}>There seems to be corrupted download. {'\n'}Please restart the application if you see the bar below stuck.</Text>}
                </View>
                }

                <View style={styles.progressBarHolder}>
                    <Progress.Bar
                        style={styles.progressBar}
                        indeterminate={this.state.indeterminate}
                        progress={this.calcProgress()}
                        color='#9E9E9E'
                    />
                </View>
                <DownloadModal
                    isVisible={this.state.isVisible}
                    downloadModalMessage={this.state.downloadModalMessage}
                    downloadModalMessage2={this.state.downloadModalMessage2}
                    downloadModalMessage3={this.state.downloadModalMessage3}
                    buttons={this.state.buttons}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: { flex: 1, backgroundColor: '#0082B3' },
    textHolderStyle: { flex: 2, justifyContent: 'flex-end', alignItems: 'center' },
    downloadInfo: { flex: 1, alignItems: 'center' },
    progressBarHolder: { flex: 1, alignItems: 'center', paddingTop: 20 },
    loadTextF: { color: 'white', fontSize: 30, paddingBottom: 20, alignItems: 'center' }
});


export default Loading;