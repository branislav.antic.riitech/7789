import React, { Component } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { filterFiles } from '../../../helpers';
import RNFB from 'rn-fetch-blob';

class Template2 extends Component {

    constructor(props) {
        super(props);

        this.images = filterFiles(this.props.data.files, 'image');
        this.videos = filterFiles(this.props.data.files, 'video');
        this.documents = filterFiles(this.props.data.files, 'document');
    }

    renderImage = () => (
        <Image resizeMethod='resize' style={styles.image} source={{ uri: `${RNFB.fs.dirs.DocumentDir}/${this.images[0].filename}` }} />
    )

    render() {
        return (
            <View style={styles.mainContainer}>
                <Text style={styles.textStyle}>{this.props.data.title}</Text>
                {this.renderImage()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    textStyle: {
        fontSize: 18,
        padding: 20
    },
    image: {
        resizeMode: 'contain',
        flex: 1,
        borderColor: 'blue'
    }
});


export default Template2;