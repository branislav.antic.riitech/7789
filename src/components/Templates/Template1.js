import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { filterFiles } from '../../../helpers';

class Template1 extends Component {

    constructor(props) {
        super(props);
        this.images = filterFiles(this.props.data.files, 'image');
        this.videos = filterFiles(this.props.data.files, 'video');
        this.documents = filterFiles(this.props.data.files, 'document');
    }

    render() {

        return (
            <View style={styles.mainContainer}>
                <Image />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'green'
    }
});


export default Template1;