import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, ScrollView } from 'react-native';
import { filterFiles } from '../../../helpers';
import RNFB from 'rn-fetch-blob';
import HTML from 'react-native-render-html';

class Template3 extends Component {

    constructor(props) {
        super(props);

        this.images = filterFiles(this.props.data.files, 'image');
        this.videos = filterFiles(this.props.data.files, 'video');
        this.documents = filterFiles(this.props.data.files, 'document');

    }

    renderImage = () => (
        <Image resizeMethod='resize' style={styles.image} source={{ uri: `${RNFB.fs.dirs.DocumentDir}/${this.images[0].filename}` }} />
    )

    render() {
        // console.log(this.props.data.text)
        // console.log(this.images)
        if(this.images.length > 1){
            this.images.map(m =>
            console.log(m))
        }
        return (
            <View style={styles.mainContainer}>
                <Text style={styles.textStyle}>{this.props.data.title}</Text>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: 2, padding: 10 }} >
                        <ScrollView>
                            <HTML html={this.props.data.text} />
                        </ScrollView>
                    </View>
                    <View style={{ flex: 4 }}>
                        {this.renderImage()}
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    textStyle: {
        fontSize: 18,
        padding: 20
    },
    image: {
        flex: 1,

    }
});


export default Template3;