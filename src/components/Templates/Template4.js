import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { filterFiles } from '../../../helpers';

class Template4 extends Component {

    constructor(props) {
        super(props);
        
        this.images = filterFiles(this.props.data.files, 'image');
        this.videos = filterFiles(this.props.data.files, 'video');
        this.documents = filterFiles(this.props.data.files, 'document');
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'brown'
    }
});


export default Template4;