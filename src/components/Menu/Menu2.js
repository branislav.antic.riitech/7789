import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import he from 'he';
import Menu3 from './Menu3';
import Menu4 from './Menu4';
import { menuClicked } from '../../actions';

class Menu2 extends Component {

    niz = () => {
        let niz = [];
        if (this.props.data.children) {
            this.props.data.children.map(m3 => {
                niz.push(m3)
                if (m3.children) {
                    m3.children.map(m4 => {
                        niz.push(m4)
                    })
                }
            })
        }

        return niz;
    }
    
    renderMenu3 = (data) => {
        if (data) {
            return data.map((m3, i) => <Menu3 key={m3.menuId} data={m3} index={i} />)
        }
    }

    isActive = () => {
        if (this.props.data.menuId === this.props.activeMenu) {
            return true;
        }
        return false;
    }

    render() {
        return (
            <View style={styles.mainContainer}>

                <View style={styles.menu2Cont}>
                    <TouchableOpacity onPress={() => this.props.menuClicked(this.props.data.menuId, this.props.data)} style={styles.menu2Item}>
                        <LinearGradient style={styles.linearStyle} colors={this.isActive() ? ['#0079AA', '#0075A8', '#005F91'] : ['#F6F7F6', '#ECEDED', '#DCDDDD']}>
                            <Text style={[styles.menu2Text, { color: this.isActive() ? '#fff' : '#424242' }]}>{he.decode(this.props.data.title)}</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>

                <View style={styles.menu3Container}>
                    {this.renderMenu3(this.niz())}
                </View>
            </View>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        activeMenu: state.menus.menu23Selected
    }
}

const styles = {
    mainContainer: {
        flex: 1,
        justifyContent: 'center',
        height: '100%',
        borderRightWidth: 1,
        borderColor: '#9e9e9e',
    },
    innerContainer: {
        flex: 0.9,
        borderRightWidth: 1,
        borderColor: '#9e9e9e',
    },
    menu2Cont: {
        // flex: 1,
        height: '18%',
        marginHorizontal: 20,
        marginBottom: 5,
        marginTop: 5
    },
    menu2Item: {
        flex: 1,
        width: 250,
    },
    linearStyle: {
        flex: 1,
        justifyContent: 'center'
    },
    menu2Text: {
        paddingLeft: 15,
        fontSize: 14,
    },
    menu3Container: {
        flex: 1,
        height: 250,
        flexWrap: 'wrap',
        marginHorizontal: 20,
    }
}

export default connect(mapStateToProps, { menuClicked })(Menu2);