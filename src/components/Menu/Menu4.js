import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import he from 'he';
import { menuClicked } from '../../actions';
import { height } from '../../../helpers';

class Menu4 extends Component {


    isActive = () => {
        if (this.props.data.menuId === this.props.activeMenu) {
            return true;
        }
        return false;
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <TouchableOpacity onPress={() => this.props.menuClicked(this.props.data.menuId)} style={styles.menu4btn}>
                    <View style={[styles.textHolder, { borderColor: this.isActive() ? '#0075A8' : '#9e9e9e' }, { borderWidth: this.isActive() ? 3 : 1 }]}>
                        <Text style={styles.text}>{he.decode(this.props.data.title)}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = {
    mainContainer: {
        height: 50,
        display: 'flex',
        paddingLeft: 20,
        margin: 5
    },
    textHolder: {
        flex: 1,
        justifyContent: 'center'
    },
    text: {
        backgroundColor: '#fff',
        color: '#424242',
        fontSize: height * 0.02,
        paddingLeft: 15,
    },
    menu4btn: {
        height: 50,
        width: 225,
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        activeMenu: state.menus.menu23Selected
    }
}

export default connect(mapStateToProps, { menuClicked })(Menu4);