import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import he from 'he';
import LinearGradient from 'react-native-linear-gradient';
import { changeMenu1, changeAndResetMenu1 } from '../../actions';

class Menu1 extends Component {

    isActive = () => {
        if (this.props.index == this.props.menu1Selected) {
            return true;
        }
        return false;
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <TouchableOpacity onPress={() => this.props.changeAndResetMenu1(this.props.index)} style={styles.touchStyle}>
                    <LinearGradient
                        style={styles.menu1Item}
                        colors={this.isActive() ? ['#0079AA', '#0075A8', '#005F91'] : ['#F6F7F6', '#ECEDED', '#DCDDDD']}
                    >
                        <Text style={[styles.menu1Text, { color: this.isActive() ? '#fff' : '#424242' }]}>
                            {he.decode(this.props.data.title)}
                        </Text>
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = {
    mainContainer: { flex: 1, },
    touchStyle: { flex: 1 },
    menu1Item: {
        flex: 1,
        width: 300,
        padding: 10,
        borderRightWidth: 1,
        borderColor: '#fff',
        justifyContent: 'center'
    },
    menu1Text: { paddingLeft: 5, fontSize: 16 }
}

const mapStateToProps = (state, ownProps) => {
    return {
        menu1Selected: state.menus.menu1Selected
    }
}

export default connect(mapStateToProps, { changeMenu1, changeAndResetMenu1 })(Menu1);