import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import he from 'he';
import Menu4 from './Menu4';
import { menuClicked } from '../../actions';
import { height } from '../../../helpers';

class Menu3 extends Component {

    renderMenu4 = () => {
        if (this.props.data.children) {
            return this.props.data.children.map((m4, i) => <Menu4 key={m4.menuId} data={m4} index={i} />)
        }
    }

    isActive = () => {
        if (this.props.data.menuId === this.props.activeMenu) {
            return true;
        }
        return false;
    }

    render() {
        return (
            <View style={[styles.mainContainer, this.props.data.depth == 4 ? { marginLeft: 20 } : '']}>
                <TouchableOpacity onPress={() => this.props.menuClicked(this.props.data.menuId)} style={[styles.menu3btn, this.props.data.depth == 4 ? { width: 230 } : '']}>
                    <View style={[styles.textHolder, { borderColor: this.isActive() ? '#0075A8' : '#9e9e9e' }, { borderWidth: this.isActive() ? 3 : 1 }]}>
                        <Text style={styles.text}>{he.decode(this.props.data.title)}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = {
    mainContainer: {

        flexWrap: 'wrap',
    },
    textHolder: {
        flex: 1,
        justifyContent: 'center'
    },
    text: {
        backgroundColor: '#fff',
        color: '#424242',
        fontSize: height * 0.02,
        paddingLeft: 15,
    },
    menu3btn: {
        width: 250,
        height: 50,
        marginRight: 10,
        marginTop: 5
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        activeMenu: state.menus.menu23Selected
    }
}

export default connect(mapStateToProps, { menuClicked })(Menu3);