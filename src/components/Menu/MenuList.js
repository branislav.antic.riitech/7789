import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import { connect } from 'react-redux';
import Menu1 from './Menu1';
import Menu2 from './Menu2';
import { saveMenuScroll1, saveMenuScroll2 } from '../../actions';

class MenuList extends Component {


    componentDidUpdate() {
        this.scrollView2.scrollToOffset({ offset: this.props.menuScroll2, animated: false })
    }

    componentDidMount() {
        setTimeout(() => {
            this.scrollView1.scrollToOffset({ animated: false, offset: this.props.menuScroll1 })
            this.scrollView2.scrollToOffset({ animated: false, offset: this.props.menuScroll2 })
        }, 0)
    }


    render() {
        // console.log('=====', global.contentJson)
        return (
            <View style={styles.mainContainer}>

                <View style={styles.menu1Holder}>
                    <FlatList
                        onMomentumScrollEnd={({ nativeEvent }) => { this.props.saveMenuScroll1(nativeEvent.contentOffset.x) }}
                        ref={ref => this.scrollView1 = ref}
                        showsHorizontalScrollIndicator={false}
                        horizontal
                        getItemLayout={(data, index) => ({ length: 300, offset: 300 * index, index })}
                        data={global.contentJson.menuTrees[0].menuTree}
                        renderItem={({ item, index }) => <Menu1 data={item} index={index} />}
                        keyExtractor={item => item.menuId}
                    />
                </View>

                <View style={styles.menu2Holder}>
                    <FlatList
                        onMomentumScrollEnd={({ nativeEvent }) => { this.props.saveMenuScroll2(nativeEvent.contentOffset.x) }}
                        ref={ref => this.scrollView2 = ref}
                        showsHorizontalScrollIndicator={false}
                        horizontal
                        data={global.contentJson.menuTrees[0].menuTree[this.props.menu1Selected].children}
                        renderItem={({ item, index }) => <Menu2 data={item} index={index} />}
                        keyExtractor={item => item.menuId}
                    />
                </View>
            </View>
        )
    }
}

const styles = {
    mainContainer: { flex: 1 },
    menu1Holder: { flex: 1, flexDirection: 'row', },
    menu2Holder: { flex: 6.5, flexDirection: 'row', flexWrap: 'wrap', backgroundColor: '#fff' }
}

const mapStateToProps = (state, ownProps) => {
    return {
        menu1Selected: state.menus.menu1Selected,
        menu2Index: state.menus.menu2Index,
        menuScroll1: state.menus.menuScroll1,
        menuScroll2: state.menus.menuScroll2
    }
}

export default connect(mapStateToProps, { saveMenuScroll1, saveMenuScroll2 })(MenuList);